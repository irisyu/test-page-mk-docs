# Blender for Biologists

This course is for biologists, learning how to make their first animation.

## Learning goals
- Recall the definition of a shader and mesh
- apply a camera transformation in Blender
- Distinguish between a volume render and a sliced view
- Create a video with Blender using the volume slicing and camera movement features.



## Learner profiles
Participants are expected to be biologists who don't necessarily have a background in 3D visualization, but have already created their own biological cartoons with 2D software such as Biorender, Adobe illustrator, PowerPoint or InkScape.

**Mehmet** is a PhD researcher using volume Electron Microscopy methods. They have made biological cartoons in Illustrator, using Biorender predrawn images. However, because their data exists in 3D, they would like to have their biological cartoons also in 3D. They tried the course and loved it and now only use Blender for everything. They have used it in making their thesis.
























